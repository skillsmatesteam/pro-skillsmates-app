export const routes = {
  root: '/',
  login: '/login',
  logout: '/logout',
  refresh: '/refresh',
  dashboard: '/dashboard',
  accounts: '/accounts',
  versions: '/versions',
  messages: '/messages',
  partners: '/partners',
  ambassadors: '/partners'
};
