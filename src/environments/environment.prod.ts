export const environment = {
  production: true,

  // INT
  // ms_accounts_host: 'https://int-skillsmates.ml:8030',
  // ms_media_host: 'https://int-skillsmates.ml:8031',
  // ms_pages_host: 'https://int-skillsmates.ml:8032',
  // ms_notifications_host: 'https://int-skillsmates.ml:8033',
  // ms_messages_host: 'https://int-skillsmates.ml:8034',
  // ms_settings_host: 'https://int-skillsmates.ml:8035',
  // ms_statistics_host: 'https://int-skillsmates.ml:8036',
  // ms_groups_host: 'https://int-skillsmates.ml:8037',
  // ms_admin_host: 'https://int-skillsmates.ml:8038',
  // aws_s3_multimedia_bucket: 'https://skillsmatesmedia.s3.us-east-2.amazonaws.com/int.multimedia/',

  // // PROD
  ms_accounts_host: 'https://skillsmates.tk:8030',
  ms_media_host: 'https://skillsmates.tk:8031',
  ms_pages_host: 'https://skillsmates.tk:8032',
  ms_notifications_host: 'https://skillsmates.tk:8033',
  ms_messages_host: 'https://skillsmates.tk:8034',
  ms_settings_host: 'https://skillsmates.tk:8035',
  ms_statistics_host: 'https://skillsmates.tk:8036',
  ms_groups_host: 'https://skillsmates.tk:8037',
  ms_admin_host: 'https://skillsmates.tk:8038',
  aws_s3_multimedia_bucket: 'https://skillsmatesmedia.s3.us-east-2.amazonaws.com/multimedia/',

  itemsPerPage : 32,
  itemsPerPageSearch : 24,
  itemsPerPageDocument : 32,
  youtube_embed_host: 'https://www.youtube.com/embed/'
};
