// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // ms_accounts_host: 'http://localhost:8030',
  ms_accounts_host: 'https://skillsmates.tk:8030',
  ms_media_host: 'http://localhost:8031',
  // ms_pages_host: 'http://localhost:8032',
  ms_pages_host: 'https://skillsmates.tk:8032',
  ms_notifications_host: 'http://localhost:8033',
  ms_messages_host: 'http://localhost:8034',
  ms_settings_host: 'http://localhost:8035',
  ms_statistics_host: 'http://localhost:8036',
  ms_groups_host: 'http://localhost:8037',
  ms_admin_host: 'http://localhost:8038',
  aws_s3_multimedia_bucket: 'https://skillsmatesmedia.s3.us-east-2.amazonaws.com/int.multimedia/',
  itemsPerPage : 32,
  itemsPerPageSearch : 24,
  itemsPerPageDocument: 32,

  // INT
  // ms_accounts_host: 'https://int-skillsmates.ml:8030',
  /*ms_media_host: 'https://int-skillsmates.ml:8031',
  ms_pages_host: 'https://int-skillsmates.ml:8032',
  ms_notifications_host: 'https://int-skillsmates.ml:8033',
  ms_messages_host: 'https://int-skillsmates.ml:8034',
  ms_settings_host: 'https://int-skillsmates.ml:8035',
  ms_statistics_host: 'https://int-skillsmates.ml:8036',
  ms_groups_host: 'https://int-skillsmates.ml:8037',
  aws_s3_multimedia_bucket: 'https://skillsmatesmedia.s3.us-east-2.amazonaws.com/int.multimedia/',
  itemsPerPage : 32,*/


  youtube_embed_host: 'https://www.youtube.com/embed/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
