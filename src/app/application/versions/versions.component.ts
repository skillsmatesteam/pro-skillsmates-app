import { Component, OnInit } from '@angular/core';
import {Alert} from '../../models/alert';

@Component({
  selector: 'app-versions',
  templateUrl: './versions.component.html',
  styleUrls: ['./versions.component.css']
})
export class VersionsComponent implements OnInit {

  componentToShow = 'list';
  alert: Alert = {} as Alert;

  constructor() { }

  ngOnInit(): void {
  }

  onClickAddVersion(): void {
    this.componentToShow = 'add';
  }

  onSaveVersionEvent(alert: Alert): void{
    this.componentToShow = 'list';
  }
}
