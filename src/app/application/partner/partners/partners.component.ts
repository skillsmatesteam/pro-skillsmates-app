import { Component, OnInit } from '@angular/core';
import {Partner} from '../../../models/partner/partner';
import {PartnersGeneralInfos} from '../../../models/partnersGeneralInfos';
import {PartnerService} from '../../../services/partner/partner.service';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.css']
})
export class PartnersComponent implements OnInit {
  loading: boolean;
  selectedCountry: string;
  partners: Partner[];
  selectedCategory: string;
  selectedDomain: string;
  city: string;
  partnersGeneralInfos: PartnersGeneralInfos = {} as PartnersGeneralInfos;
  constructor(private partnerService: PartnerService) { }

  ngOnInit(): void {
    this.findPartnersInfos();
  }

  findPartnersInfos(): void {
    this.loading = true;
    this.partnerService.findPartnersGeneralInfos().subscribe((response) => {
      this.partnersGeneralInfos = response.resource;
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
    });
  }
  onChangeCategory(e): void {
    this.loading = true;
    this.selectedCategory = e.target.value.toLowerCase();
    // this.filter();
    this.loading = false;
  }

  onChangeCountry(e): void {
    this.loading = true;
    this.selectedCountry = e.target.value.toLowerCase();
    // this.filter();
    this.loading = false;
  }

  onChangeDomain(e): void {
    this.loading = true;
    this.selectedDomain = e.target.value.toLowerCase();
    // this.filter();
    this.loading = false;
  }

  onKeyupCity(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  /*filter(): void {
    this.partners = PartnerHelper.filterPartnersByName(
    this.filterPartnersByCountry(this.filterPartnersByStatus(this.allPartners)), this.searchContent);
  }*/

  /*filterPartnersByCountry(partnersToFilter: Partner[]): Partner[] {
    let filteredPartners: Partner[];
    if (this.selectedCountry && this.selectedCountry !== 'tous') {
      this.partnerGeneralInfos.countries.forEach(elt => {
        if (elt.label.toLowerCase() === this.selectedCountry) {
          filteredPartners = partnersToFilter.filter((partner: Partner) =>
            partner.country && partner.country.label.toLocaleLowerCase() === this.selectedCountry);
        }
      });
    } else {
      filteredPartners = partnersToFilter;
    }
    return filteredPartners;
  }*/
}
