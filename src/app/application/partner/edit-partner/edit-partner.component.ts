import { Component, OnInit } from '@angular/core';
import {InfosPartner} from '../../../models/partner/infosPartner';
import {PartnerHelper} from '../../../helpers/partner-helper';
import {PartnersGeneralInfos} from '../../../models/partnersGeneralInfos';
import {PartnerService} from '../../../services/partner/partner.service';
import {Partner} from '../../../models/partner/partner';
import {Alert} from '../../../models/alert';

@Component({
  selector: 'app-edit-partner',
  templateUrl: './edit-partner.component.html',
  styleUrls: ['./edit-partner.component.css']
})
export class EditPartnerComponent implements OnInit {
  selectedInfoPartner: InfosPartner;
  positionInfo: number;
  loading: boolean;
  isfirtTemplate: boolean;
  submitted = false;
  errors;
  selectedCountry: string;
  raisonSociale: string;
  partner: Partner = {} as Partner;
  siren: string;
  selectedDomain: string;
  infosPartner = PartnerHelper.infosPartner;
  partnersGeneralInfos: PartnersGeneralInfos = {} as PartnersGeneralInfos;
  alert: Alert = {} as Alert;

  constructor(private partnerService: PartnerService) { }

  ngOnInit(): void {
    this.isfirtTemplate = true;
    this.selectedInfoPartner = PartnerHelper.infosPartner[0];
    this.positionInfo = this.selectedInfoPartner.position;
    this.findPartnersInfos();
    this.errors = {valid: true, name: '', website: '', password: '', email: '', phoneNumber: '', newPassword: '', confirmPassword: ''};
  }

  onClickTab(infoPartner: any): void {
    this.isfirtTemplate = true;
    this.selectedInfoPartner = infoPartner;
    this.positionInfo = infoPartner.position;
    //this.filterMediaSubtypes();
    //this.dismissPreview = false;
  }
  onChangeCountry(e): void {
    this.loading = true;
    this.selectedCountry = e.target.value.toLowerCase();
    // this.filter();
    this.loading = false;
  }
  onChangeDomain(e): void {
    this.loading = true;
    this.selectedDomain = e.target.value.toLowerCase();
    // this.filter();
    this.loading = false;
  }

  findPartnersInfos(): void {
    this.loading = true;
    this.partnerService.findPartnersGeneralInfos().subscribe((response) => {
      this.partnersGeneralInfos = response.resource;
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
    });
  }

  createPartner(): void {
    this.partnerService.createPartner(this.partner).subscribe((response) => {
      this.partner = response.resource;
      this.loading = false;
    }, (error) => {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Biographie non sauvegardée'};
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }

  onKeyupRaisonSociale(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupSiren(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupCity(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupCountry(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupDescription(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupPhone(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupAddress(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupEmail(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupInstagram(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupTiktok(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupFacebook(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onKeyupWebSite(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }

  onKeyupLinkedIn(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }

  onKeyupSnapChat(): void {
    this.loading = true;
    // this.filter();
    this.loading = false;
  }
  onClickNext(): void {
    this.isfirtTemplate = false;
  }

  onClickSave(): void {
    this.createPartner();
  }
}
