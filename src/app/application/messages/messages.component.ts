import { Component, OnInit } from '@angular/core';
import {Assistance} from '../../models/assistance';
import {AssistanceService} from '../../services/assistance/assistance.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  loading: boolean;
  display = 'list';
  assistance: Assistance = {} as Assistance;
  constructor() { }

  ngOnInit(): void {
  }

  onClickMessage(message: Assistance): void {
    this.assistance = message;
    this.display = 'message';
  }

  onClickReturn(label: string): void{
    this.display = 'list';
  }
}
