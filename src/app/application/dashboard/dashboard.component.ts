import { Component, OnInit } from '@angular/core';
import {SkillsmatesCard} from '../../models/skillsmatesCard';
import {AccountsService} from '../../services/account/accounts.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  postCard: SkillsmatesCard = {} as SkillsmatesCard;
  constructor(private accountService: AccountsService) { }

  ngOnInit(): void {

    this.postCard.icon = 'fa fa-user-o';
    this.postCard.title = 'Comptes';
    this.postCard.nb = 0;
    this.postCard.titleClass = 'widget-title1';
    this.postCard.bgClass = 'dash-widget-bg1';
  }

  onClickMigrate(): void {
    this.accountService.migrate().subscribe((response) => {
      console.log(response);
    }, (error) => {
      console.log(error);
    });
  }
}
