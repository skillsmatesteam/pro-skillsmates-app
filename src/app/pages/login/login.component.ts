import { Component, OnInit } from '@angular/core';
import {RoutesHelper} from '../../helpers/routes-helper';
import {Alert} from '../../models/alert';
import {StringHelper} from '../../helpers/string-helper';
import {variables} from '../../../environments/variables';
import {Router} from '@angular/router';
import {AuthenticateService} from '../../services/account/authenticate.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  account: Account = {} as Account;
  email: string;
  password: string;
  loading: boolean;
  checked: boolean;
  submitted = false;
  alert: Alert = {} as Alert;
  errors;
  routes = RoutesHelper.routes;
  id: string;
  constructor(private authenticateService: AuthenticateService,
              private router: Router) { }

  ngOnInit(): void {
    this.initErrors();
    this.authenticateService.logout();
    this.checked = false;
    this.loading = false;
  }

  initErrors(): void {
    this.submitted = false;
    this.errors = {valid: true, firstname: '', lastname: '', email: '', password: '', confirmPassword: '', cgu: ''};
  }

  validateEmail(email): boolean {
    return StringHelper.validateEmail(email);
  }

  keyupEmail(): void {
    this.errors.email = '';
  }

  keyupPassword(): void {
    this.errors.password = '';
  }

  onClickLogin(): void {
    this.loading = true;
    if (this.validateAccountForLogin()) {
      this.authenticateService.authenticate(this.email, this.password).subscribe((response) => {
        this.account = response.resource;
        localStorage.setItem(variables.account_current, JSON.stringify(this.account));
        this.router.navigate([this.routes.get('dashboard')]);
      }, (error) => {
        console.log(error);
        this.alert = {display: true, class: 'danger', title: 'Attention!', message: error.error.message};
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Les valeurs entrées sont invalides'};
      this.loading = false;
    }
  }

  validateAccountForLogin(): boolean {
    this.initErrors();
    if (!this.validateEmail(this.email)) {
      this.errors.valid = false;
      this.errors.email = 'Email invalide';
    }

    if (this.password === undefined || this.password === '') {
      this.errors.valid = false;
      this.errors.password = 'Mot de passe requis';
    }

    if (this.password && this.password.length < 5) {
      this.errors.valid = false;
      this.errors.password = 'Mot de passe doit être de 5 caractéres minimum';
    }
    this.submitted = true;
    return this.errors.valid;
  }

}
