import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticateService} from '../../services/account/authenticate.service';
import {ToastService} from '../../services/toast/toast.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  loading: boolean;
  constructor(private authenticateService: AuthenticateService,
              // private accountService: AccountService,
              private toastService: ToastService,
              // private webSocketService: WebSocketService,
              private router: Router) { }

  ngOnInit(): void {
    this.loading = true;
    // this.webSocketService.closeWebsocket();
    // this.accountService.logoutAccount().subscribe((response) => {
    //   this.logout();
    // }, (error) => {
    //   this.logout();
    // });

    this.logout();
  }

  logout(): void{
    // this.authenticateService.logout();
    // this.toastService.showSuccess('Déconnexion', 'Deconnexion avec succces');
    this.loading = false;
    this.router.navigate(['/']);
  }

}
