import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {VersionDescription} from '../../../models/versionDescriptions';
import {VersionService} from '../../../services/version/version.service';
import {Alert} from '../../../models/alert';

@Component({
  selector: 'app-add-version',
  templateUrl: './add-version.component.html',
  styleUrls: ['./add-version.component.css']
})
export class AddVersionComponent implements OnInit {
  version: string;
  description: string;
  versionDescription: VersionDescription = {} as VersionDescription;
  alert: Alert = {} as Alert;
  @Output() saveVersionEvent = new EventEmitter<Alert>();

  constructor(private versionService: VersionService) { }

  ngOnInit(): void {
    this.versionDescription.descriptions = [];
  }

  onClickValidate(): void {
    if (this.description && this.description.length > 0){
      this.versionDescription.descriptions.push(this.description);
    }
    this.description = '';
  }

  onClickDelete(description: string): void {
    if (description && description.length > 0){
      const index = this.versionDescription.descriptions.indexOf(description);
      if (index && index > -1){
        this.versionDescription.descriptions.splice(index, 1);
      }
    }
  }

  onClickValidateVersion(): void {
    this.versionDescription.version = this.version;
    if (this.validateVersion()){
      this.saveVersion();
    }else {
      this.alert = {
        display: true,
        class: 'danger',
        title: 'Erreur',
        message: '  Les données entrées sont invalides'
      };
    }
  }

  saveVersion(): void{
    this.versionService.saveVersion(this.versionDescription).subscribe((response) => {
      this.alert = {
        display: true,
        class: 'success',
        title: 'Felicitations !!!',
        message: '  Version enregistrée'
      };
      this.saveVersionEvent.emit(this.alert);
    }, (error => {
      this.alert = {
        display: true,
        class: 'danger',
        title: 'Erreur',
        message: '  ' + error.message
      };
    }));
  }

  validateVersion(): boolean{
    return this.versionDescription &&
      this.versionDescription.version &&
      this.versionDescription.descriptions &&
      this.versionDescription.descriptions.length > 0;
  }

  resetAlert(): void {
    this.alert = {
      display: false,
      class: '',
      title: '',
      message: ''
    };
  }

  onKeyupDescription(): void {
    this.resetAlert();
  }

  onKeyupVersion(): void {
    this.resetAlert();
  }
}
