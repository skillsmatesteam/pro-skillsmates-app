import { Component, OnInit } from '@angular/core';
import {Version} from '../../../models/version';
import {VersionDescription} from '../../../models/versionDescriptions';
import {VersionService} from '../../../services/version/version.service';

@Component({
  selector: 'app-versions-list',
  templateUrl: './versions-list.component.html',
  styleUrls: ['./versions-list.component.css']
})
export class VersionsListComponent implements OnInit {

  versions: Version[] = [];
  versionDescriptions: VersionDescription[] = [];
  constructor(private versionService: VersionService) { }

  ngOnInit(): void {
    this.findAllVersions();
  }

  trackByFunc(index, value): any{
    return value.id;
  }

  findAllVersions(): void{
    this.versionService.findAllVersions().subscribe((response) => {
      this.versions = response.resources;
      this.sortVersions();
    }, (error) => {});
  }

  onClickAccordion(key, versionDescription: VersionDescription): void {
    // this.resetAccordion();
    if (!versionDescription.showBody) {
      versionDescription.showBody = true;
      versionDescription.accordionClass = 'collapseAccordion';
    } else {
      versionDescription.showBody = false;
      versionDescription.accordionClass = 'expandAccordion';
    }
  }

  resetAccordion(): void{
    this.versionDescriptions.forEach(versionDescription => versionDescription.showBody = false);
  }

  sortVersions(): void {
    this.versions.forEach(version => {
      let versionDescription: VersionDescription;
      const  position = this.findCorrectVersion(version);
      if (position >= 0 ){
        versionDescription = this.versionDescriptions[position];
        versionDescription.descriptions.push(version.description);
        this.versionDescriptions.splice(position, 1);
      }else {
        versionDescription = {} as VersionDescription;
        versionDescription.version = version.version;
        versionDescription.descriptions = [];
        versionDescription.descriptions.push(version.description);
      }
      this.versionDescriptions.push(versionDescription);
    });
  }

  findCorrectVersion(version: Version): number{
    let position = -1;
    this.versionDescriptions.forEach((versionDescription, index) => {
      if (versionDescription.version === version.version){
        position = index;
      }
    });
    return position;
  }

}
