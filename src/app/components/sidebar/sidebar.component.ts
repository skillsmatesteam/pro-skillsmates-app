import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {RoutesHelper} from '../../helpers/routes-helper';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  route: string;
  routes = RoutesHelper.routes;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.route = this.router.url;
  }

  onClickSidebar(): void {
    this.route = this.router.url;
  }

}
