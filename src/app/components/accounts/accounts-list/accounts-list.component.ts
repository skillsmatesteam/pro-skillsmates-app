import { Component, OnInit } from '@angular/core';
import {Account} from '../../../models/account';
import {StringHelper} from '../../../helpers/string-helper';
import {AccountsService} from '../../../services/account/accounts.service';

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.css']
})
export class AccountsListComponent implements OnInit {

  loading: boolean;
  accounts: Account[] = [];
  allAccounts: Account[] = [];
  roles: string[] = [];
  selectedRole: string;
  nameSearch: string;
  emailSearch: string;

  constructor(private accountService: AccountsService) { }

  ngOnInit(): void {
    this.initRoles();
    this.findAccounts();
  }

  initRoles(): void {
    this.roles.push('Admin');
    this.roles.push('Utilisateur');
  }

  findAccounts(): void{
    this.loading = true;
    this.accountService.findAllAccounts().subscribe((response) => {
      this.accounts = response.resources;
      this.allAccounts = this.accounts;
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 20);
  }

  role(account: Account): string{
    return account && account.role && account.role === 'USER' ? 'Utilisateur' : 'Admin';
  }

  onChangeRole(role: string): void {
    // console.log(role);
    // this.accounts = [];
    // this.allAccounts.forEach((elt, index) => {
    //   if (elt.role.toLowerCase().includes(role.toLowerCase())){
    //     this.accounts.push(elt);
    //   }
    // });
    //
    // console.log(this.accounts);
  }

  onKeyupSearchAccountName(): void {
    this.accounts = [];
    this.allAccounts.forEach((elt, index) => {
      if (elt.firstname.toLowerCase().includes(this.nameSearch.toLowerCase()) ||
        elt.lastname.toLowerCase().includes(this.nameSearch.toLowerCase())){
        this.accounts.push(elt);
      }
    });
  }

  onKeyupSearchAccountEmail(): void {
    this.accounts = [];
    this.allAccounts.forEach((elt, index) => {
      if (elt.email.toLowerCase().includes(this.emailSearch.toLowerCase())){
        this.accounts.push(elt);
      }
    });
  }
}
