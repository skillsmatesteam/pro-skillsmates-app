import {Component, Input, OnInit} from '@angular/core';
import {SkillsmatesCard} from '../../models/skillsmatesCard';

@Component({
  selector: 'app-skillsmates-card',
  templateUrl: './skillsmates-card.component.html',
  styleUrls: ['./skillsmates-card.component.css']
})
export class SkillsmatesCardComponent implements OnInit {

  @Input() skillsmatesCard: SkillsmatesCard;
  constructor() { }

  ngOnInit(): void {
  }

}
