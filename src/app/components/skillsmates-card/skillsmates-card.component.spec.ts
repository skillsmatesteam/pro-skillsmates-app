import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillsmatesCardComponent } from './skillsmates-card.component';

describe('SkillsmatesCardComponent', () => {
  let component: SkillsmatesCardComponent;
  let fixture: ComponentFixture<SkillsmatesCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillsmatesCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillsmatesCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
