import { Component, OnInit } from '@angular/core';
import {Account} from '../../models/account';
import {variables} from '../../../environments/variables';
import {StringHelper} from '../../helpers/string-helper';
import {RoutesHelper} from '../../helpers/routes-helper';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loggedAccount: Account = {} as Account;
  routes = RoutesHelper.routes;

  constructor() { }

  ngOnInit(): void {
    const param: any = localStorage.getItem(variables.account_current);
    this.loggedAccount = JSON.parse(param);
  }

  public name(): string{
    return StringHelper.truncateName(this.loggedAccount, 18);
  }

  onClickProfileMenu(): void {

  }
}
