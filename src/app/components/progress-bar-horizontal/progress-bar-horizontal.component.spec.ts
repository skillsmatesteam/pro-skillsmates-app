import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressBarHorizontalComponent } from './progress-bar-horizontal.component';

describe('ProgressBarHorizontalComponent', () => {
  let component: ProgressBarHorizontalComponent;
  let fixture: ComponentFixture<ProgressBarHorizontalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressBarHorizontalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressBarHorizontalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
