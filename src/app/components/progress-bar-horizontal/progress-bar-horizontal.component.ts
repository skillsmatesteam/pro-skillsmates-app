import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-progress-bar-horizontal',
  templateUrl: './progress-bar-horizontal.component.html',
  styleUrls: ['./progress-bar-horizontal.component.css']
})
export class ProgressBarHorizontalComponent implements OnInit {
  @Input() loading;
  constructor() { }

  ngOnInit(): void {
  }

}
