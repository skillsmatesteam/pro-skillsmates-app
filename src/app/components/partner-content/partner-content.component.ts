import {Component, Input, OnInit} from '@angular/core';
import {RoutesHelper} from '../../helpers/routes-helper';
import {AuthenticateService} from '../../services/account/authenticate.service';
import {Account} from '../../models/account';
import {Partner} from '../../models/partner/partner';

@Component({
  selector: 'app-partner-content',
  templateUrl: './partner-content.component.html',
  styleUrls: ['./partner-content.component.css']
})
export class PartnerContentComponent implements OnInit {

  loading: boolean;
  loggedAccount: Account = {} as Account;
  @Input() partner: Partner = {} as Partner;
  routes = RoutesHelper.routes;

  constructor(private authenticationService: AuthenticateService) { }

  ngOnInit(): void {

    this.loggedAccount = this.authenticationService.getCurrentAccount();
  }
}
