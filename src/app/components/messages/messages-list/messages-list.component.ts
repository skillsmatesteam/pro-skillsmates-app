import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Assistance} from '../../../models/assistance';
import {AssistanceService} from '../../../services/assistance/assistance.service';
import {Account} from '../../../models/account';
import {StringHelper} from '../../../helpers/string-helper';

@Component({
  selector: 'app-messages-list',
  templateUrl: './messages-list.component.html',
  styleUrls: ['./messages-list.component.css']
})
export class MessagesListComponent implements OnInit {

  loading: boolean;
  messagesAssistance: Assistance[] = [];
  @Output() assistanceEvent = new EventEmitter<Assistance>();
  constructor(private assistanceService: AssistanceService) { }

  ngOnInit(): void {
    this.findMessagesAssistance();
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 18);
  }

  findMessagesAssistance(): void{
    this.loading = true;
    this.assistanceService.findAssistances().subscribe((response) => {
      this.messagesAssistance = response.resources;
    }, (error) => {
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }

  onClickMessage(message: Assistance): void {
    this.assistanceEvent.emit(message);
  }

}
