import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Assistance} from '../../../models/assistance';
import {StringHelper} from '../../../helpers/string-helper';
import {Account} from '../../../models/account';

@Component({
  selector: 'app-message-content',
  templateUrl: './message-content.component.html',
  styleUrls: ['./message-content.component.css']
})
export class MessageContentComponent implements OnInit {

  @Output() assistanceEvent = new EventEmitter<string>();
  @Input() message = {} as Assistance;
  constructor() { }

  ngOnInit(): void {
  }

  onClickReturn(): void{
    this.assistanceEvent.emit('list');
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 50);
  }

}
