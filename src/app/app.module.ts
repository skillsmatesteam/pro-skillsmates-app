import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { ImagePipe } from './pipes/image.pipe';
import {AuthenticateService} from './services/account/authenticate.service';
import {FormsModule} from '@angular/forms';
import { LoadingComponent } from './components/loading/loading.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { DashboardComponent } from './application/dashboard/dashboard.component';
import { RootComponent } from './application/root/root.component';
import { HeaderComponent } from './components/header/header.component';
import { NotificationsBoxComponent } from './components/notifications-box/notifications-box.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AvatarPipe } from './pipes/avatar.pipe';
import { SkillsmatesCardComponent } from './components/skillsmates-card/skillsmates-card.component';
import { VersionsComponent } from './application/versions/versions.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { MessagesComponent } from './application/messages/messages.component';
import {AssistanceService} from './services/assistance/assistance.service';
import {HttpConfigInterceptor} from './interceptor/httpConfig.interceptor';
import { MessageContentComponent } from './components/messages/message-content/message-content.component';
import { MessagesListComponent } from './components/messages/messages-list/messages-list.component';
import { AlertComponent } from './components/alert/alert.component';
import { AccountsComponent } from './application/accounts/accounts.component';
import { AccountsListComponent } from './components/accounts/accounts-list/accounts-list.component';
import {AccountsService} from './services/account/accounts.service';
import { PartnersComponent } from './application/partner/partners/partners.component';
import { PartnerContentComponent } from './components/partner-content/partner-content.component';
import {PartnerService} from './services/partner/partner.service';
import { AvatarPartnerPipe } from './pipes/avatar-partner.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { EditPartnerComponent } from './application/partner/edit-partner/edit-partner.component';
import { FooterComponent } from './components/footer/footer.component';
import { EditProfileImageComponent } from './components/edit-profile-image/edit-profile-image.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import { ProgressBarHorizontalComponent } from './components/progress-bar-horizontal/progress-bar-horizontal.component';
import {VersionService} from './services/version/version.service';
import { AddVersionComponent } from './components/version/add-version/add-version.component';
import { VersionsListComponent } from './components/version/versions-list/versions-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ImagePipe,
    LoadingComponent,
    DashboardComponent,
    RootComponent,
    HeaderComponent,
    NotificationsBoxComponent,
    SidebarComponent,
    AvatarPipe,
    SkillsmatesCardComponent,
    VersionsComponent,
    LogoutComponent,
    MessagesComponent,
    MessageContentComponent,
    MessagesListComponent,
    AlertComponent,
    AccountsComponent,
    AccountsListComponent,
    PartnersComponent,
    PartnerContentComponent,
    AvatarPartnerPipe,
    TruncatePipe,
    EditPartnerComponent,
    FooterComponent,
    EditProfileImageComponent,
    ProgressBarHorizontalComponent,
    AddVersionComponent,
    VersionsListComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ImageCropperModule
    ],
  providers: [
    AuthenticateService,
    AssistanceService,
    AccountsService,
    PartnerService,
    VersionService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
