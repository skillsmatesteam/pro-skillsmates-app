import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Partner} from '../../models/partner/partner';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {
  url = '/skillsmates-admin/partners';
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'application/json; charset=utf-8');
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  /**
   * find partners for general infos
   */
  findPartnersGeneralInfos(): Observable<any>{
    return this.httpClient.get<any>(environment.ms_admin_host + this.url + '/infos', this.httpOptions);
  }

  /**
   * create an partner
   */
  createPartner(partner: Partner): Observable<any>{
    return this.httpClient.post<any>(environment.ms_admin_host + this.url, partner);
  }
}
