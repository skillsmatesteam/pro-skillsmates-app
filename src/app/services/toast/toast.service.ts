import { Injectable } from '@angular/core';
import {variables} from '../../../environments/variables';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  // constructor(private toastr: ToastrService) {}
  //
  // showSuccess(title?: string, message?: string): void {
  //   this.toastr.success(message, title);
  // }
  //
  // showError(title: string, message: string): void {
  //   this.toastr.error(message, title);
  // }
  //
  // showGeneralError(): void {
  //   this.toastr.error('Une erreur est survenue', 'Erreur');
  // }
  //
  // showWarning(title: string, message: string): void {
  //   this.toastr.warning(message, title);
  // }
  //
  // showInfo(title: string, message: string): void {
  //   this.toastr.info(message, title);
  // }
}
