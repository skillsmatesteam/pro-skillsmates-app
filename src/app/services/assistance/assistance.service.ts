import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Assistance} from '../../models/assistance';

@Injectable({
  providedIn: 'root'
})
export class AssistanceService {
  url = '/skillsmates-settings/assistance';
  httpHeaders = new HttpHeaders();
  params: HttpParams = new HttpParams();
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  sendAssistance(assistance: Assistance): Observable<any>{
    return this.httpClient.post<any>(environment.ms_settings_host + this.url, assistance, this.httpOptions);
  }

  findAssistances(): Observable<any>{
    return this.httpClient.get<any>(environment.ms_settings_host + this.url, this.httpOptions);
  }
}
