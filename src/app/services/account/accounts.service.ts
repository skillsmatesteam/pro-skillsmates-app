import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  url = '/skillsmates-accounts/accounts';
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'application/json; charset=utf-8');
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  /**
   * find all accounts
   */
  findAllAccounts(): Observable<any> {
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url, this.httpOptions);
  }

  migrate(): Observable<any> {
    return this.httpClient.get<any>(environment.ms_pages_host + '/skillsmates-pages/social-interaction/migrate', this.httpOptions);
  }
}
