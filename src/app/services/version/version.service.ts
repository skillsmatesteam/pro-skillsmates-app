import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {VersionDescription} from '../../models/versionDescriptions';

@Injectable({
  providedIn: 'root'
})
export class VersionService {
  url = '/skillsmates-admin/versions';
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'application/json; charset=utf-8');
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  /**
   * find all accounts
   */
  findAllVersions(): Observable<any> {
    return this.httpClient.get<any>(environment.ms_admin_host + this.url, this.httpOptions);
  }

  saveVersion(versionDescription: VersionDescription): Observable<any> {
    return this.httpClient.post<any>(environment.ms_admin_host + this.url, versionDescription, this.httpOptions);
  }
}
