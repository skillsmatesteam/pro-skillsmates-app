import {Country} from './country';
import {Partner} from './partner/partner';
import {BaseModel} from './baseModel';
import {PartnerType} from './partnerType';
import {ActivitySector} from './activitySector';
import {Category} from './category';
import {ActivityArea} from './activityArea';


export class PartnersGeneralInfos extends BaseModel{
  constructor(){
    super();
  }

  partners: Partner[];

  countries: Country[];

  partnerType: PartnerType[];

  /*activitiesSector: ActivitySector[];*/

  activitiesAreas: ActivityArea[];

  categories: Category[];
}
