import {BaseModel} from './baseModel';

export class Country extends BaseModel{
  constructor(){
    super();
  }

  label: string;
}
