import {BaseModel} from './baseModel';

export class Subscription extends BaseModel{
    constructor(){
        super();
    }
    follower: Account;
    followee: Account;
}
