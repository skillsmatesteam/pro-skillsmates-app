export class SkillsmatesCard {
    nb: number;
    title: string;
    icon: string;
    titleClass: string;
    bgClass: string;
}
