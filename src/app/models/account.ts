import {BaseActiveModel} from './baseActiveModel';
import {Gender} from './gender';
import {Country} from './country';
import {ProfessionalStatus} from './professionalStatus';
import {Subscription} from './subscription';

export class Account extends BaseActiveModel{
    constructor(){
        super();
    }
    firstname: string;
    lastname: string;
    email: string;
    address: string;
    phoneNumber: string;
    birthdate: Date;
    password: string;
    confirmPassword: string;
    description: string;
    gender: Gender;
    biography: string;
    status: ProfessionalStatus;
    country: Country;
    city: string;
    follower: number;
    following: number;
    publication: number;
    currentEstablishmentName: string;
    currentJobTitle: string;
    profilePicture: string;
    connected: boolean;
    subscription: Subscription;
    role: string;
}
