import {BaseActiveModel} from './baseActiveModel';
import {Account} from './account';

export class Version extends BaseActiveModel{
    constructor(){
        super();
    }
  version: string;
  description: string;
}
