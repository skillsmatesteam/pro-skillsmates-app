import {BaseModel} from './baseModel';

export class Gender extends BaseModel{
  constructor(){
    super();
  }

  label: string;
}
