import {BaseModel} from './baseModel';

export class ProfessionalStatus extends BaseModel{
  constructor(){
    super();
  }

  label: string;
}
