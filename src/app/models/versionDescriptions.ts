import {BaseModel} from './baseModel';

export class VersionDescription extends BaseModel{
    constructor(){
        super();
    }
  version: string;
  descriptions: string[] = [];
  showBody: boolean;
  accordionClass: string;
}
