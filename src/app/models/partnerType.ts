import {BaseModel} from './baseModel';

export class PartnerType extends BaseModel{
  constructor(){
    super();
  }

  label: string;
}
