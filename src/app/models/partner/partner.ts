import {BaseActiveModel} from '../baseActiveModel';
import {Country} from '../country';
import {PartnerType} from '../partnerType';
import {ActivationStart} from '@angular/router';
import {Category} from '../category';
import {ActivityArea} from '../activityArea';

export class Partner extends BaseActiveModel{
    constructor(){
        super();
    }
    name: string;
    address: string;
    email: string;
    phoneNumber: string;
    description: string;
    partnerType: PartnerType ;
    city: string;
    country: Country;
    profilePicture: string;
    category: Category;
    activityArea: ActivityArea;
    siren: string;
    website: string;
    facebook: string;
    instagram: string;
    tiktok: string;
    linkedin: string;
    snapchat: string;
}
