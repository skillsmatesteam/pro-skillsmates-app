import {BaseModel} from '../baseModel';

export class Category extends BaseModel{
    constructor(){
        super();
    }
  label: string;
}
