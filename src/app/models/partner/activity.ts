import {Partner} from './partner';
import {BaseActiveModel} from '../baseActiveModel';

export class Activity extends BaseActiveModel{
    constructor(){
        super();
    }

  title : string;
  description : string;
  website : string;
  picture : string;
  partner : Partner;
}
