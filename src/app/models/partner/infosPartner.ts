import {BaseModel} from '../baseModel';

export class InfosPartner extends BaseModel{
    constructor(){
        super();
    }
  color: string;
  position: number;
  description: string;
}
