import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {RootComponent} from './application/root/root.component';
import {AuthenticationGuardService} from './services/authentication.guard.service';
import {DashboardComponent} from './application/dashboard/dashboard.component';
import {VersionsComponent} from './application/versions/versions.component';
import {LogoutComponent} from './pages/logout/logout.component';
import {MessagesComponent} from './application/messages/messages.component';
import {AccountsComponent} from './application/accounts/accounts.component';
import {PartnersComponent} from './application/partner/partners/partners.component';
import {EditPartnerComponent} from './application/partner/edit-partner/edit-partner.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full' },
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: '', component: RootComponent, canActivate: [AuthenticationGuardService], children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'accounts', component: AccountsComponent},
      {path: 'versions', component: VersionsComponent},
      {path: 'messages', component: MessagesComponent},
      {path: 'partners', component: EditPartnerComponent},
      {path: 'ambassadors', component: PartnersComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
