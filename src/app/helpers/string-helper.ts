import {RoutesHelper} from './routes-helper';
import {routes} from '../../environments/routes';
import {Account} from '../models/account';
import {variables} from '../../environments/variables';

export class StringHelper {
  routes = RoutesHelper.routes;

  static truncateName(account: Account, nb: number): string{
    let name = ' ' + account.firstname + ' ' + account.lastname;
    if (name.length > nb){
      name = name.slice(0, nb - 1) + '...';
    }
    return name;
  }

  static toNumber(str: string): number {
    return +str;
  }

  static getMaxFileSize(): number{
    return variables.max_file_size * 1048576;
  }

  static formatBytes(bytes, decimals = 2): string {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  static truncateText(text: string, nb: number): string{
    if (text && text.length > nb){
      text = text.slice(0, nb - 1) + '...';
    }
    return text;
  }

  static validateEmail(email: string): boolean{
    const regExp = /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/;
    return regExp.test(email);
  }

  static validateUrl(url): boolean{
    const regExp = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    return regExp.test(url);
  }

  static urlToTab(url: string): string{
    let tab = routes.dashboard;
    if (url.indexOf(routes.dashboard) >= 0){
      tab = routes.dashboard;
    }
    return tab;
  }

  static getProfessionalIcon(account: Account): string{
    if (account && account.status){
      if (account.status.label.toLowerCase() === 'professionnel'){
        return 'company';
      }

      if (account.status.label.toLowerCase() === 'enseignant'){
        return 'school';
      }

      if (account.status.label.toLowerCase() === 'etudiant'){
        return 'school';
      }
    }
    return 'company';
  }
}
