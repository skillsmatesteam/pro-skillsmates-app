import {routes} from '../../environments/routes';

export class RoutesHelper {
  static routes: Map<string, string> = new Map<string, string>([
    ['root', routes.root],
    ['login', routes.login],
    ['logout', routes.logout],
    ['refresh', routes.refresh],
    ['dashboard', routes.dashboard],
    ['accounts', routes.accounts],
    ['versions', routes.versions],
    ['messages', routes.messages],
    ['partners', routes.partners],
    ['ambassadors', routes.partners]
  ]);
}
