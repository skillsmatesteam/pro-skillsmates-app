import {Partner} from '../models/partner/partner';
import {InfosPartner} from '../models/partner/infosPartner';


export class PartnerHelper {

  static infosPartner: InfosPartner[] = [
    {id: 0, idServer: '', color: '#16162D',  position : 1,  description : 'A propos'},
    {id: 0, idServer: '', color: '#16162D',  position : 2,  description : 'Engagements & Actions d\'accompagnement'},
    {id: 0, idServer: '', color: '#16162D',  position : 3,  description : 'Activités'}
  ];


  /**
   * filter partners by name (name) that contain searchContent
   * @param partnersToFilter: Partners to filter
   * @param searchContent: the filter string
   */
  /*static filterPartnersByName(partnersToFilter: Partner[], searchContent): Partner[]{
    let filteredPartners: Partner[];
    if (searchContent){
      filteredPartners = partnersToFilter.filter( (partner: Partner) =>
        searchContent && partner.firstname && partner.lastname &&
        (partner.lastname.toLocaleLowerCase() + ' ' + partner.firstname.toLocaleLowerCase()).indexOf(searchContent.toLowerCase()) >= 0);
    }else {
      filteredPartners = partnersToFilter;
    }
    return filteredPartners;
  }*/

  /**
   * filter partners by city
   * @param partners: partners to filter
   * @param city: filter string
   */
  /*static filterPartnersMessagesByName(partnersMessagesToFilter: PartnerMessage[], searchContent): PartnerMessage[]{
    let filteredPartners: PartnerMessage[];
    if (searchContent){
      filteredPartners = partnersMessagesToFilter.filter( (partnerMessage: PartnerMessage) =>
        searchContent && partnerMessage.partner.firstname && partnerMessage.partner.lastname &&
        // tslint:disable-next-line:max-line-length
        (partnerMessage.partner.lastname.toLocaleLowerCase() + ' ' + partnerMessage.partner.firstname.toLocaleLowerCase()).indexOf(searchContent.toLowerCase()) >= 0);
    }else {
      filteredPartners = partnersMessagesToFilter;
    }
    return filteredPartners;
  }*/

}
