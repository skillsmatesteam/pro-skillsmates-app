import { Pipe, PipeTransform } from '@angular/core';
import {environment} from '../../environments/environment';
import {Account} from '../models/account';

@Pipe({
  name: 'avatar'
})
export class AvatarPipe implements PipeTransform {

  transform(account: Account, ...args: unknown[]): unknown {
    if (account && account.profilePicture) {
      return environment.aws_s3_multimedia_bucket + account.idServer + '/' + account.profilePicture.toLowerCase();
    } else {
      return './assets/images/user.svg';
    }
  }
}
