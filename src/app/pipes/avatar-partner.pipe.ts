import { Pipe, PipeTransform } from '@angular/core';
import {Partner} from '../models/partner/partner';
import {environment} from '../../environments/environment';

@Pipe({
  name: 'avatarPartner'
})
export class AvatarPartnerPipe implements PipeTransform {

  transform(partner: Partner, ...args: unknown[]): unknown {
    if (partner && partner.profilePicture) {
      return environment.aws_s3_multimedia_bucket + partner.idServer + '/' + partner.profilePicture.toLowerCase();
    } else {
      return './assets/images/user.svg';
    }
  }
}
